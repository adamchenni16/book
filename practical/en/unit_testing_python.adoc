﻿:source-highlighter: prettify
:source-highlighter: highlightjs

= TP: Build + Tests + Continuous Integration
Régis WITZ
:doctype: book
:toc:
:toc-title:
:toclevels: 1

image::https://i.creativecommons.org/l/by-nc-nd/3.0/88x31.png[Creative Commons License, link="http://creativecommons.org/licenses/by-nc-nd/3.0/fr/"]


== Prerequisites

This TP requires a computer with functional installations of https://www.python.org/[Python] (≥2.7), https://www.w3schools.com/python/python_pip.asp[pip] and https://tox.readthedocs.io/en/latest/[tox].

If `tox` software is not installed on your TP machine, install it using the python package manager `pip`.
Even if you are not admin, you can enter the following command:
[source,bash]
----
pip install --user tox
----
By default, `tox` will thus be installed in the `$HOME/.local/bin` directory.
You can then invoke tox with the command `~/.local/bin/tox`.
In order to use the `tox` command directly, you need to add `~/.local/bin` to your `PATH` environment variable, for example by entering the following command in your console or `~/.bashrc` file:
[source,bash]
----
export PATH=$PATH:~/.local/bin
----



== Introduction: FizzBuzz

FizzBuzz is an algorithmic problem you can encounter during job interviews.
It is not very complicated in itself, and just allows recruiters not to waste time with candidates whose computer skills seem too limited.

A common description of this problem is as follows:
[quote, 'https://en.wikipedia.org/wiki/Fizz_buzz[Wikipédia]']
____
Write a program that displays numbers from 1 to 100.
However, for multiples of 3, display "Fizz" instead of the number.
For multiples of 5, display "Buzz" instead of the number.
For numbers that are both multiples of 3 and 5, display "FizzBuzz".
____

For example, a correct answer to this problem would start as follows:
[source]
----
1, 2, Fizz, 4, Buzz, Fizz, 7, 8, Fizz, Buzz, 11, Fizz, 13, 14, FizzBuzz, 16, 17, Fizz, 19, Buzz, ...
----

Before you rush to (easily) find the solution to this problem on the Internet, I recommend that you take a moment to think about it.
During a job interview, you may only have your brain at your disposal.
So, seize every opportunity to train it a little!



== Exercise 1: Configuration, build and unit testing

. Create a working directory.
  To archive what you'll do in this TP and let you go back while reviewing your previous TP on source management, I advise you to make this directory a local Git repository and commit what you've done at each step of this TP.

. Your directory will become a Python project.
  To do this, create at the root of your directory a `setup.py` file similar to this one:
+
[source,python]
.setup.py
----
# -*- coding: utf-8 -*-
from setuptools import setup, find_packages

setup(
    name = 'fizzbuzz',
    version = '1.0.0',
    url = 'https://github.com/you/fizzbuzz.git',
    author = 'You',
    author_email = 'you@yourmailprovider.whatever',
    description = 'TP 02',
    packages = find_packages(),    
)
----

. Then, create a `tox.ini` file.
  This file contains the configuration of the automation tool we will use: https://tox.readthedocs.io/en/latest/[tox].
+
[source,ini]
.tox.ini
----
# create this in the same directory as your setup.py
[tox]
# list here the environments in which you want to test your project
envlist = py27, py36

[testenv]
# list all the dependencies of your project here
# for now, we'll only need one test library: pytest
deps = pytest
# list the commands to be run
# Here we're just running our tests
commands = pytest
----

. Execute the `tox` command.
  Read and understand the output of this command, and the files and folders it generated.
  If you don't understand everything, call your teacher, who will be happy to explain.

. So, you understand that there are several things that need to be fixed.
  First, there are two runtime environments listed in `tox.ini`: "python 2.7", and "python 3.6".
  However, you probably don't have exactly the same version(s) of python installed on your system. +
  By checking the status of your installation with commands like `ls -ls /usr/bin/python*` or `python --version`, if necessary, correct the `envlist` property of your `tox.ini`. +
  Then run the `tox' command again. What has changed?

. Next, you need to create the "skeleton" of your project.
  Create a `fizzbuzz` directory, then create the `fizzbuzz.py` file in it:
  (if you have changed the name of your project in your `setup.py` file, you should change the name of this directory to remain consistent).
+
[source,python]
.fizzbuzz/fizzbuzz.py
----
# -*- coding: utf-8 -*-

class FizzBuzz:

    def convert(self, number):
        raise NotImplementedError
----

. Finally, you need to create a *unit test module* that will validate the quality of your project.
  Create the `test_fizzbuzz.py` file:
+
[source,python]
.fizzbuzz/test_fizzbuzz.py
----
# -*- coding: utf-8 -*-
from unittest import TestCase
from fizzbuzz import FizzBuzz

class FizzBuzzTest(TestCase):
    def test_returns_number_for_input_not_divisible_by_3_or_5(self):
        self.fizzbuzz = FizzBuzz()
        self.assertEqual('1',  self.fizzbuzz.convert(1))
        self.assertEqual('2',  self.fizzbuzz.convert(2))
        self.assertEqual('4',  self.fizzbuzz.convert(4))
        self.assertEqual('7',  self.fizzbuzz.convert(7))
        self.assertEqual('11', self.fizzbuzz.convert(11))
        self.assertEqual('13', self.fizzbuzz.convert(13))
        self.assertEqual('14', self.fizzbuzz.convert(14))
----

. Restart `tox`.
  Notice that your unit test, `test_returns_number_for_input_not_divisible_by_3_or_5`, is running.
  However, this test fails.
  Why did it fail?

. If you were to implement the `FizzBuzz` class now, the only test method present, `test_returns_number_for_input_not_divisible_by_3_or_5`, would not be sufficient to guarantee the quality of your implementation.
  Based on the specification of the FizzBuzz problem given in the introduction to this TP, write the missing test methods. +
  Any hint? +
  The specification tells you to make four different displays, depending on the input parameter.
  Currently, only one of these four cases is being tested.
  Thus, you should create at least three new test methods.

. Implement the `FizzBuzz` class itself.
  You can only consider your implementation correct when the `tox'` command tells you at least the 4 proper tests have been run, with 0 errors.
  Of course, you are not allowed to delete, comment or pass the execution of any of these tests.

. If you are using `git` (and that's _exactly what you are doing_, amirite?), once you have completed the implementation and all the tests are successful, this is a good time to commit your changes.
  Be careful, not everything in your project folder needs to be committed!

. Currently, if your unit tests cover the whole functional scope and are successes, the functional capacity of your project is guaranteed.
  However, not every feature of the FizzBuzz problem is *tested in isolation*.
  After listening to your teacher explaining what this means, *refactor* your code by removing the line `self.fizzbuzz = FizzBuzz()` from each of your test functions, and by adding to your test class an initialization method `setUp` and a cleanup method `tearDown`.
+
[source,python]
.fizzbuzz/test_fizzbuzz.py
----
# -*- coding: utf-8 -*-

# (imports)

class FizzBuzzTest(TestCase):

    def setUp(self):
        self.fizzbuzz = FizzBuzz()

    def tearDown(self):
        self.fizzbuzz = None

    # (vos 4 fonctions de test)
----



== Exercise 2: Code coverage

. In each instruction, one or more bugs can hide.
  How can we be sure that we are actually testing _all_ our lines of code? +
  To do so, we will test the *code coverage* of our tests, thanks to the https://coverage.readthedocs.io/en/v4.5.x/[coverage] module. +
  Modify your `tox.ini` file as follows:
+
[source,ini]
.tox.ini
----

# ...

[testenv]
# add the dependency to coverage, ...
deps =
    pytest
    coverage
# ... and modify our run command like this:
commands =
    coverage erase
    coverage run -m pytest
    coverage report -m
----

. Run the `tox` command again.
  It's not really easy to read, is it? +
  To configure the `coverage` module, create the `.coveragerc` file:
+
[source,ini,title='.coveragerc']
----
[run]
source = fizzbuzz/
omit = setup.py
       .tox/*
----

. Run the `tox` command again.
  So, do you reach 100%? ლ(｀ー´ლ)



== Exercise 3: Coding convention(s)

. Your code must be correct and fully tested.
  Furthermore, it must be _readable_, and the best way to do this is to follow the *coding convention* of your platform.
  In python, such a convention is https://www.python.org/dev/peps/pep-0008/[PEP 8].
  Did you, during this TP, write "nice" Python?
  That's what we are going to find out.

. In the same way as in exercise 2, add a new dependency to your project: `flake8`.
  Then, add the following section at the end of your `tox.ini` file.
+
[source,ini]
.tox.ini
----
[testenv:flake8]
commands=
    flake8 --count --show-source
----
+
Note that when creating a new section like this, `flake8` must also be added to your `envlist`.
Compare this with the way you used `coverage` in the previous exercise.

. Run the `tox` command again.
  Fix your code until `flake8` no longer finds anything wrong with your code.
  Congratulations, you can be proud of yourself! (•̀ᴗ•́)و ̑̑



== Exercise 4: Mocks

At this point in the TP, the `FizzBuzz` class is functional and fully tested.
However, it doesn't fully answer the original problem, which specifies to _display_ the answers corresponding to numbers from 1 to 100.
It is therefore necessary to implement, and therefore test, a `ProblemSolver` utility class which will complete the work.

image::TP-02.4.1.png[caption="", title="`ProblemSolver` only knows the `Int2String` and `Displayer` interfaces; it does not know any concrete implementation, and in particular doesn't know `FizzBuzz`."]

. Create a new module, `solver`, declaring the abstract classes `Int2String` and `Displayer`, and the concrete class `ProblemSolver`.

. Create a new testing module `test_problemsolver.py`.
  This module should test an object which subclasses `ProblemSolver` _WITHOUT_ giving it actual instantiated objects as parameter.
  Instead, you should give it mocks instantiated using the module https://docs.python.org/3/library/unittest.mock-examples.html[unittest.mock].
  To help you, here is the `test_problemsolver.py` module "skeleton":
+
[source,python]
.fizzbuzz/test_problemsolver.py
----
# -*- coding: utf-8 -*-
from unittest import TestCase
from unittest.mock import patch
from solver import ProblemSolver

# ... mock_convert and mock_display definitions

class ProblemSolverTest(TestCase):

    def setUp(self):
        with patch('solver.Int2String') as mock:
            mock.convert = mock_convert
            converter = mock
        with patch('solver.Displayer') as mock:
            mock.display = mock_display
            displayer = mock
        self.solver = ProblemSolver(converter, displayer)

# ... test methods validating calls to self.solver.solve
----

== Exercise 5: Continuous integration

The next practical work will allow you to do something relatively similar to what this exercise describes, but in a simpler case.
It may therefore be beneficial for you to come back to this exercise after completing the next TP.

The goal is to learn how to set up a *continuous integration server* through https://github.com/[GitHub] and one of its third-party services: https://travis-ci.org/[Travis CI].
If you haven't already done so, feel free to take the time necessary to familiarize yourself with their interfaces, their possibilities but also their limitations.

If you have, as I've advised, used a local Git repository, you can point it to a remote GitHub repository and then "push" it.

Then, you'll need to reorganize your project files according to the https://docs.pytest.org/en/latest/goodpractices.html[best practices] of `setuptools`, `pytest` and `tox`.

Finally, you will need to create the `.travis.yml` file at the root of your repository, to tell Travis CI how to build your project.
Here is an example to get you started:
[source]
.travis.yml
----
# You won't be needing the administration rights for this exercise.
# However, this can be useful if, for example, you need to install on
# your continuous integration server additional packages with apt.
# Don't worry, continuous integration servers offered by services
# like Travis CI are actually virtual machines that exist
# just long enough to build your app. So, if you "break" something
# with your admin rights, it will only have consequences on your build,
# and none of the other service users.
# You will only need to fix your configuration and push
# your modifications to try your luck again with a new VM!
sudo: false

# Your project is in python, so you need python on your server.
language: python
# Thanks to a continuous integration server, you can build your project
# in different environments, even if those are not present on
# your development machine.
# For example, you can request that your server has all 3 versions
# of python directly installed and, by modifying tox.ini like this...
# envlist = py{27,35,36},flake8
# ... run 3 different builds in 3 different python environments, in order
# for your code to be compatible with as many different users as possible.
python:
  - "2.7"
  - "3.5"
  - "3.6"

# Before the build, Travis CI lets you configure/install
# additional components on your continuous integration server.
# For this exercise, you will need to install tox at the very least.
install: pip install tox

# This last directive describes the commands to run the build.
# Since all the details are handled in your tox.ini, you can just run tox.
# That's the advantage of using such a tool.
# If you want to build everything by hand (without tox or other similar tool),
# You need to put your various instructions here
# (or possibly write a dedicated script and call it from here).
script: tox
----

You can also do a lot of other interesting things with this kind of system.
For example, you can generate documentation for your classes with a tool like https://pdoc3.github.io/pdoc/[pdoc].
Then, you can host the automatically generated documentation directly online thanks to https://pages.github.com/[GitHub Pages].

Note that if you want to build projects for Windows systems, this is not possible with Travis CI.
To do so, you can use a specialized service, such as https://www.appveyor.com/[AppVeyor].
The configuration is done there in a way almost similar to Travis CI.



== Références

* https://docs.python.org/3/library/unittest.mock.html[Documentation] for module `unittest.mock`.
* https://setuptools.readthedocs.io/en/latest/[Python Setuptools] (your `setup.py` comes from there).
* tox https://tox.readthedocs.io/en/latest/examples.html[configuration and samples].
* A version numbering method you can use in your projects: https://semver.org/[semantic versioning].
* https://coverage.readthedocs.io/en/v4.5.x/[Documentation] for `coverage`.
* The https://www.python.org/dev/peps/pep-0008/[PEP 8] code convention, as long as a little explanation about https://medium.com/python-pandemonium/what-is-flake8-and-why-we-should-use-it-b89bd78073f2[why it is important].
* How to https://docs.python.org/3/library/unittest.mock-examples.html[write mocks].
* Documentation of python https://docs.python.org/3/library/abc.html[abc (Abstract Base Classes)].
  And a https://www.python-course.eu/python3_abstract_classes.php[short tutorial] too, while we're at it.
