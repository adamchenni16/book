== Definitions

=== The project

A project is a *unique* process, which consists of a set of coordinated and controlled *activities*,
with start and end dates, undertaken in order to achieve goals consistent with specific *quality* requirements,
all the while respecting  constraints of *costs* and *delays*.

image::quality/QCT.png[caption="Figure 01:", title="Quality, Budget and Time are interdependent"]

These quality, cost and time constraints determine the *perimeter* of the project.
They are interdependent: "winning" against one often means "losing" against at least one of the others.

image::quality/NOPE.png[caption="Figure 02:", 500px, title="\"I want the best, the cheapest, and I don't want to wait at all!\""]

All *activities* of a project create *deliverables*: tools, methods or services.

=== The actors and their roles

The project is carried out primarily by men and women.
These persons are grouped into legal _entities_
(a particular company, department or team, ...).

Each of these entities has one or more responsibilities
(plan, design, develop, test, ...)
which, if carried out correctly, will lead to the success of the project.

During the lifecycle of a project, two particular entities can be distinguished:

* the *customer*: also called _Project owner_ (_PO_) or, specifically in French, _Maîtrise d'ouvrage_ (_MOA_).
  It is the one who *expresses the need*.
* the *supplier*: also called _Maîtrise d'œuvre_ (_MOE_) in French.
  It is the one that *meets the need*.

In addition to the customer and its supplier, other entities may be *stakeholders* in the project.
These include the end users of the solution, funding agencies, and so on.

[[roles_moa]]
==== The customer

The customer is at the origin of the project.
It is the customer who sets the objectives of the project, i.e. the *need* to be fulfilled, as well as the *costs* and *deadlines* of the project.

It is the customer who has the "*functional knowledge*" of the field in which the project takes place,
such as the professional and technical contexts in which the project must exist,
and the expectations about all the functionalities the solution will have to include.

This is the *owner* of the project.
The customer is therefore *the one who pays*.

[[roles_moe]]
==== The supplier

The supplier provides the product expected by the customer.
It ensures that quality, costs and deadlines are met.

It can in turn be assisted by one or more suppliers.
It is then responsible for coordinating and supervising all of them.
