= Génie Logiciel - Support de cours
Régis WITZ <regis.witz@free.fr>
:doctype: book
:toc: left
:toc-title: Table des Matières
:toclevels: 2
:imagesdir: resources

image::CC_BY-NC-ND_88x31.png[Creative Commons License, link="http://creativecommons.org/licenses/by-nc-nd/3.0/fr/"]


include::chapters/intro.adoc[]

include::chapters/quality.adoc[]

include::chapters/lifecycle.adoc[]

include::chapters/pert.adoc[]

include::chapters/uml.adoc[]

include::chapters/tests.adoc[]

include::chapters/ci.adoc[]

include::../practical/fr/master_list.adoc[]

include::chapters/references.adoc[]

include::chapters/remerciements.adoc[]

include::chapters/legal.adoc[]
