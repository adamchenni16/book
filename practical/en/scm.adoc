:source-highlighter: prettify
:source-highlighter: highlightjs
:icons: font
:linkattrs:

= Version Control
Régis WITZ
:doctype: book
:toc:
:toc-title:
:toclevels: 1

image::https://i.creativecommons.org/l/by-nc-nd/3.0/88x31.png[Creative Commons License, link="http://creativecommons.org/licenses/by-nc-nd/3.0/fr/"]


== Prerequisites

These exercises require you to link:https://git-scm.com/book/fr/v2/D%C3%A9marrage-rapide-Installation-de-Git[have `git` installed] on your computer.
You should have at least a text editor, too, or better, a link:https://www.softwaretestinghelp.com/best-code-editor/[code editor].

Make sure you satisfy these prerequisites before starting these exercises.

== Introduction
link:https://en.wikipedia.org/wiki/Version_control[*Version control systems*,title=Wikipedia page on version control] (also called source control, or _VCS_), as its name implies, allows to create *versions* of a source code, which has three goals:

* Have a *history* of modifications done to the code base.
  Keeping a constant record of who did what, when and why in the code is important for the *maintainability* of that code.
* Allow to *go backwards*.
  No one is perfect; sometimes, despite the precautions taken, unwanted changes find their way into the code.
  Similarly, sometimes the understanding of how a feature should be implemented is only really understood during implementation, and kind of "lost" afterwards.
* And, of course, most importantly: making *collaborative work* easy.
  With the notable exception of link:https://en.wikipedia.org/wiki/Pair_programming[_pair programming_,title=Page wikipedia on pair programming], it is rather rare that several developers can modify the same sections of the source code without "stepping on each other's toes". A VCS makes it possible to work together asynchronously, merging everyone's changes in an operation called *merge*.

These three objectives are fully achieved if each user publishes as often as possible, by putting in each publication, or *commit*, the modifications related to one and only one feature.
Each of these modifications should be as atomic, understandable and correct as possible, while remaining relevant.
This working discipline can be summed up by the maxim "_commit early, commit often_".

So let's discover link:https://en.wikipedia.org/wiki/Git[Git,title=Wikipedia page of Git], such a version control software.
The specific benefits of Git are the following:

* The fact that it's *distributed* software brings flexibility, as long as security.
  Indeed, in case of loss of the central server that most organizations set up, it is easy to start from the most recent repository existing within the development team.
  Moreover, it is easy for some developers to operate "on the fringe" of the central repository.
* In most cases, `git` is *fast*.
  Operations that can take several minutes to several hours with other _VCS_ often take link:https://gist.github.com/emanuelez/1758346[less than a second,title=Quantitative git performance analysis].
* Git provides a versioned workspace for each developer.
  This allows everyone to manage their own history before sharing it with others.
  Git is therefore perfectly *adapted to working alone*.
* Git is well known and link:https://stackshare.io/git[used in many organizations,title=List of Git users].
  This adoption rate has led to *many third-party tools* that enrich the experience or make the work of its users easier.
* Git is currently the closest to a *standard* version management tool, as it is at the heart of services such as GitLab, GitHub, and many others.


[NOTE]
.Other version control systems
=====
Git is ultimately just one VCS among many.
Even though it's the most widely used today in many contexts, that doesn't mean it's used _everywhere_.
For a large code base, moving from one VCS to another is not trivial, and losing its history is often unacceptable.
As a result, you may have to use other solutions.
For your career, I therefore advise you to test some alternatives, or at least to know their specificities.

Some examples are:

* link:https://www.mercurial-scm.org/[*Mercurial* (hg),title=Mercurial website], very similar to Git.
  If you know Git, Mercurial shouldn't be a problem for you.
* link:https://subversion.apache.org/quick-start[*Subversion* (svn),title=Subversion webite] has been the "standard" for a very long time.
  You may still encounter it in the wild.
* link:http://cvs.nongnu.org/[*CVS* (Concurrent Version System),title=CVS website] is an even older "standard".
  It is now totally outdated, but ... you never know!
* link:https://www.perforce.com/[*Perforce*,title=Perforce website] is a proprietary solution that has its merits.
  It has the preference of link:https://www.perforce.com/customers[several companies,title=Perforce user list, on the Perforce website], especially in the video game development community.

Most of the concepts that apply to Git apply, although differently, to its alternatives.
If you want to learn about an alternative, you'll need to transfer the knowledge you've acquired here.
Generally speaking, comparing alternatives is a reflex you should have.
It's the best way to choose the solution that best suits _your_ needs.
=====

[WARNING]
.Version control ≠ Configuration management
=====
Note that version management should not be confused with configuration management (or _SCM_).
Configuration management covers all the processes and tools used to compile, package and deploy software.
It covers a variety of softwares, and in particular one or more version controllers.
However, the scope of configuration management is much broader.

We'll talk about this later.
=====



== Branching

Even with the asynchronous teamwork capabilities provided by a SCM, it is rare (and not recommended) for an entire feature to be released at once.
It is therefore necessary, in order not to hinder other members of the development team in their own feature implementation, to always work on a copy of the source code.
In the context of version control, this "copy" of the source code is called a *branch*.
The default branch of the source code is called *master* (or *main*, or sometimes *trunk*).

Since a branch can be created from any other branch, there is a risk of _merge_ conflicts or even loss of entire functionalities if branches are not properly tracked and managed.
It is therefore essential that every development team agrees on a *strategy* to manage their branches.
Two strategies are common:

* the so-called "stable trunk, development branches" strategy, described in detail in link:http://nvie.com/posts/a-successful-git-branching-model[_A successful Git branching model_,title=An article explaining the motivation behind Git flow],
* the so-called "unstable trunk, delivery branches", described in link:https://barro.github.io/2016/02/a-succesful-git-branching-model-considered-harmful[_A successful Git branching model considered harmful_,title=The seemingly exact oposite opinion to the article "A successful Git branching model"].

[NOTE]
.« ... "considered harmful"?
=====
Neither strategy is inherently better than the other.
Even though the opinions of some professionals can sometimes be very clear-cut and claim the opposite, the most important thing is _to have a common strategy_.
Specifically, that strategy needs to be one that the members of the development team agree with, and one that will allow them to do their job.

Everything else is a matter of personal habits and preferences.
=====

In the following, we'll deal mainly with the first of these two strategies, as it is supported by a very handy `git` _wrapper_: link:https://danielkummer.github.io/git-flow-cheatsheet/[*git-flow*,title=git-flow cheatsheet].
However, even with git flow, the actual strategy applied will depend above all on the discipline of the development team.
In addition, it will remain possible (and even essential in some cases) to create delivery branches, as we will see.

=== `git flow`

The use of `git flow` requires a standardization of the branches.
Two of them are permanent:

* `master` represents the version of the software currently in production (current version).
* `develop` centralizes all features to be made available in the next release.

Three other branches, or rather types of branches, have a temporary lifespan:

* `feature/` groups all _commits_ related to the development of a particular feature.
  When that feature is finished, and it needs to be included in the next release,
  such a branch is created from `develop`, and merged into `develop`.
* `release/` is used to prepare for the next release.
  It can be used for testing (especially validation or recipe testing), or to fix the latest bugs.
  It is created from `develop`, and merged into `master` and `develop`.
* `hotfix/` is used to fix bugs found in production.
  It is created from `master`, and merged into `master` and `develop`.



== In practice

The following can be worked on individually as well as in groups.

* in solo mode, the different paths used will simply be local.
* in two-player mode (or more),
  you'll need to use the network addresses of your peers.
  (using urls such as `user@machine:/path/to/repo`),
  or take turns on the same machine,
  and coordinate with them as the operations need to be carried out. +
  For example, here is the sequence of actions to be carried out if you're working in pairs.

** the first performs steps 1 to 7, 10 to 12, and then 15.
** the second performs steps 8 to 14.
** Then the roles are reversed!


=== Exercise 1: Basic Commands

. Create an empty repository in a new directory.
  Observe and try to understand the `.git` directory that was created.

. Enter your personal information for your repository.

. Create a source file in your repository.
  Use whatever programming language you want, it doesn't matter and just needs to compile. +
  For example, the following Python file does the job very well:
+
[source,python]
.src/main.py
----
#!/usr/bin/python

if __name__ == '__main__':
	print "Hello, world"
----
You can do this practical work with C++, too:
+
[source,c]
.src/main.cpp
----
#include <iostream>

int main(int argc, char **argv) {
	::std::cout << argv[0] << ::std::endl;
}
----


. Compile your source file. +
  For example, to compile the previous `src/main.py` file:
+
....
python [-O] -m py_compile src/main.py
....
The équivalent command to compile `src/main.cpp` is just:
+
....
g++ [-O3] src/main.cpp
....

. Observe the status of your repository.
  Which files are desirable to keep track of?
  Which ones should not be kept by the source manager?
  Why not?

. Create a `.gitignore' file at the root of your repository so that Git will ignore certain files.
  Always check the effect of your `.gitignore' on the status of your repository.

. Add your source file and the `.gitignore' file to your repository index.
  Observe the status of your repository. +
  Then commit these two files.
  Look again at the status of your repository. +
  Finally, take a look at your repository history.

. Make a copy of your first repository.
  Be careful, the directory where this second repository is located must of course be
  _outside_ of the directory where the first repository is located.
  The best is to put the two repositories side by side. +
  To make it easier to understand what happens next, enter for this second repository
  some personal information that is different from that of your first repository.

. Observe the status of the second repository.
  Note the absence of uncommitted files. +
  Check the path this second repository points to.

. Add a line to the source file of your first repository.
  Commit these changes. +
  Then retrieve the changes from your first repository into your second repository.
  View the history of your second repository.

. Delete the line you added previously from the source file of your first repository.
  Commit these changes. +
  Then retrieve the changes from your first repository into your second repository.
  View the history of your second repository.

. Modify at least one line in the source file of your first repository.
  Then, see the difference with the last committed version.
  Finally, commit your changes. +
  Edit _the same line in the same file_ in your second repository.
  Then, see the difference with the last committed version.
  Finally, commit your changes; be careful, so that you understand the next step better,
  it is preferable for the comment of this commit in your second repository
  to be different from the commit comment you just made in your first repository. +
  For example, you can modify the file of your first repository in this way:
+
[source,python]
.depot1/main.py
----
	print "Hello, world!!!"
----
And the file in your second repository like this:
+
[source,python]
.depot2/main.py
----
	print "Hello, world???"
----
. Try to get the changes from your first repository into your second repository: there is a conflict.
  Look at the status of your second repository to confirm the conflict.

. Open the conflicting file in your second repository.
  Locate the start (`<<<<<`) and end (`>>>>>>> VERSION`) markers of the conflict. +
  Decide how to resolve the conflict.
  You can keep the code from either or both of your repositories.
  However, once you have made your choice, it is necessary to remove the conflict markers
  `<<<<<<<`, `============` and `>>>>>>>>>>>>`. +
  Finally, add the modified file to the index and commit it.

. Retrieve the changes from your second repository into your first repository.
  Compare the history of your first repository with that of your second repository.



=== Exercise 2: cherry-pick

Sometimes when indexing changes,
the developer notices that a file contains changes to be indexed,
but also one or more changes that they do not want to be indexed. +
Git solves this problem by indexing only some changes and not others.

* Edit multiple lines in your source file.
  You might better understand if the modified lines are not contiguous.
  (for example, add one line at the beginning of your file and another at the end).

* Add this file to the index using the `--patch` option.
  Observe how this option allows you to add only some of your changes.
  In particular, try the `e` option.



=== Exercise 3: stash

Sometimes a developer has to interrupt his work when it is not ready to be validated, nor can it be lost, in order to (for example) fix a bug urgently (_ie._ do a hotfix). +
As we'll see in the next exercise, creating a branch is one solution to this problem, but Git offers a second option.

Study the different uses of the `git stash` command [link:https://git-scm.com/book/en/v2/Git-Tools-Stashing-and-Cleaning[documentation,title=Documentation for stash]].
Learn how to use it, so that you can interrupt your development at any time to work on something else, commit, and recover previously interrupted work.



=== Exercise 4: branching

Creating, merging, and deleting *branches* is very easy with Git.

Use Exercise 1 as an inspiration to apply what you've learned to the branches.
For example:

. Create a branch.
  Edit a source file on that branch, or create a new file on it.
  Commit your change to the branch.
  Go back to the `master` and merge the branch into it.
  Delete the branch.

. Do the same, while you have to deal with a conflict.

Then use `*git flow*` to familiarize yourself with its workflow.
If you're in one of the following situations, enter the corresponding `git flow` commands.
Each time, try to understand the `git` commands that your `git flow` command does for you!

. Develop a new feature for the next version of the software.
. Prepare a new version of the software.
. Correct a bug in the current version.
  Don't forget to check that this fix is well propagated also to your fellow developers!
. Correct a bug reported in the *previous* version of the software. +
  Hint: `git flow` alone might not be enough!



=== Exercise 5: Patches

Members of a development team do not always have access to a Git server to centralize their work.
Also, some of these developers may _not_ want to go to a server to share their work. +
In this case, the most commonly used communication medium is the *patch*.
A patch file contains all changes made to a repository so that another repository can benefit from them.

* Create a branch in your first repository.
  Make a few commits to this branch (whatever the content).

* Create a patch file with the following command:
+
....
git format-patch master --stdout > my_modifications.patch
....

* From your second repository, watch the changes proposed by the patch,
  but _without applying them_, with the following command:
+
....
git apply --stat /path/to/my_modifications.patch
....

* From your second repository, check if the patch is applied correctly
  with the following command (if there is no output, it's good):
+
....
git apply --check /path/to/my_modifications.patch
....

* From your second repository, apply the patch with the following command:
+
....
git am --signoff /path/to/my_modifications.patch
....

* Analyze the history of your second repository.
  What effect had the `--signoff` option on commit comments?
  Why is it useful?


=== Exercise 6: Altering History

Study the many things that can be done `git rebase -i` command.

It allows you to edit existing commits, either by altering their comments or even directly editing their content.

Another "need" met by this command is to link:https://www.ekino.com/articles/comment-squasher-efficacement-ses-commits-avec-git[*squash*,title=Little guide to using the git squash command] multiple commits into one.
What advantages, and possibly disadvantages, do you see in this?
Think about it a bit, maybe with your colleagues, and then discuss it with your teacher. 😉


=== Exercise 7: Graphical user interfaces

See a sample GUI for `git`: *gitk*.
To do this, simply run the `gitk` command from inside your repository.

You'll get a more complete view of how Git concepts are translated graphically
if you have previously done at least Exercises 1 and 2.

gitk is obviously just a graphical front-end for `git` link:https://git-scm.com/downloads/guis/[among others,title=List of several graphical interfaces for git].
If you prefer to work with a graphical interface, take the time to find the solution that works best for you.
Some criteria of choice: features, ergonomics, supported platforms, price, performance, license, ...

Keep in mind however that, during your career, you may find yourself in situations where it will be impossible for you to use a graphical tool.
For example, if you need to urgently troubleshoot a production server, you will sometimes only have the command line at your disposal.


=== Exercise 8: Git server(s)

If your development environment allows it (network and possibly admin rights), set up a Git server.
You can coordinate yourself with another student to validate that it works properly.

The documentation can be found link:https://git-scm.com/book/it/v2/Git-on-the-Server-Setting-Up-the-Server[here,title=Documentation on configuring a Git server].

Although, for your own benefit, I recommend that you set up your own Git server, for this course, a quick alternative is to use *hosting services*.

==== Web Hosting

Today there are many hosting services such as link:https://gitlab.com/[GitLab] or link:https://github.com/[GitHub].
Host your existing Git repository on one of these two sites and explore the features. +
In particular, try:

* link your commits to previously created _issues_.
  What are issues?
  In what way(s) can they help teamwork?
* manage *pull requests*.
  In what way(s) can they help teamwork?
* set up a *continuous integration server* to compile code each time it is pushed.



== Going further

The following articles will allow you, through concrete use cases of Git,
to learn more about the issues, including security issues,
that can occur in a professional setting.

* A https://sethrobertson.github.io/GitBestPractices/[good practices guide,title=Good practices related to Git] related to Git and version control in general.
* A link:https://mikegerwitz.com/papers/git-horror-story[very interesting article,title=Experience of Mike Gerwitz in convincing himself of the need to sign his commits] on Git and identity theft.
  You can also find link:https://www.linux.com/tutorials/pgp-web-trust-core-concepts-behind-trusted-communication/[more details,title=Article from linux.com about PGP keys and how to use them to sign your commits] on how to build a *trust network* thanks to PGP keys.
* How to link:https://help.github.com/articles/removing-sensitive-data-from-a-repository/[go back,title=Article from GitHub's help with altering the history of an existing repository] if you've inadvertently committed sensitive information.
* A much-used way to link:https://semver.org/[identify your software versions,title=Semantic versioning] in a way that is understandable to your users.
* Using Git is convenient. Using Git with link:https://github.com/Arkweid/lefthook[Lefthook,title=Lefthook's GitHub repository] is even more convenient.
* link:https://gitmoji.carloscuesta.me/[gitmoji,title=Gitmoji's website], a fun way to quickly identify visually what a particular _commit_ brings in.



== Cheat sheet

=== Initialization

* Fill in personal information:
+
....
git config [--global] user.name "Obi-Wan Kenobi"
git config [--global] user.email "kenobi@jedicouncil.org"
....

* Activate syntax coloring:
+
....
git config [--global] color.ui true
....

* Change the editor that `git` uses,
  especially for writing commit comments,
  but also during interactive mode:
+
....
git config [--global] core.editor EDITOR
....

* Initialize an empty repository:
+
....
git init [PATH]
....

* Duplicate an existing repository:
+
....
git clone REMOTE_PATH [DST_PATH]
....

=== Modification

* Add a file to the index:
+
....
git add [--patch] PATH
....

* Validate changes to the index in a new version:
+
....
git commit [-m DESCRIPTION_MESSAGE] [PATH]
....

* Retrieve changes from another repository:
+
....
git pull [BRANCH]
....

* Remove a file from the index:
+
....
git reset HEAD [PATH]
....

* Undo local changes to a file:
+
....
git checkout -- PATH
....

* Delete a file:
+
....
git rm PATH
....

=== Analysis

* View the status of a repository:
+
....
git status
....

* View the differences with the (latest) version:
+
....
git diff [VERSION]
....

* View the differences with the index:
+
....
git diff --staged
....

* View version history:
+
....
git log [--pretty=oneline]
....

* Know who changed which line in a file, when, and why:
+
....
git blame <file>
....

* View where a repository points to:
+
....
git remote -v
....

* Adjust your history in many interesting ways ... (*squash*, *edit*, *reword*):
+
....
git rebase -i [<hash>]
....
This can also be counterproductive, even dangerous!



=== `git flow`

You can find these operations link:https://danielkummer.github.io/git-flow-cheatsheet/index.fr_FR.html[on this page,title=Useful git flow commands].

* Initialize `git flow`:
+
....
git flow init
....

* Begin development of a new feature:
+
....
git flow feature start <description>
....
This creates a branch named `feature/<description>`, and switches to it.

* Finish developing a feature:
+
....
git flow feature finish <description>
....
Not to be done without testing the branch!

* Start correcting an anomaly in production:
+
....
git flow hotfix start <description>
....
This creates a branch named `hotfix/<description>`, and switches to it.

* Acknowledge a bug fix:
+
....
git flow hotfix finish <description>
....
Make sure that you have indeed corrected the anomaly, and that you haven't created another one!

* Start the validation of a new version:
+
....
git flow release start <version>
....
This creates a branch named `release/<version>`, and switches you to it.

* Finish releasing a new version:
+
....
git flow release finish <version>
....
Make sure you have fully validated the software!


=== Branches (without git flow)

* Create a branch:
+
....
git branch BRANCH_NAME
....

* View the current branch:
+
....
git branch
....

* Change the current branch:
+
....
git checkout BRANCH_NAME
....

* Merge a branch into the current branch:
+
....
git merge BRANCH_NAME
....

* Delete a local branch:
+
....
git branch -d BRANCH_NAME
....



== References

* Quick git documentation: https://git.github.io/git-reference/
* Full git documentation: https://git-scm.com/documentation
* git cheat sheet: https://rogerdudler.github.io/git-guide/index.html
* Automatically create `.gitignore` files: https://gitignore.io (check that the result is fine for your project!)
* Did you do crap with your repository? http://ohshitgit.com

